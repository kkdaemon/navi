#include <cstdio>
#include <string>

void log(const std::string& message)
{
    printf("%s\n", message.c_str());
}
