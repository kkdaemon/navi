#include "opengl.h"

#include "logger.h"

#include <fmt/format.h>

#include <cstdio>
#include <vector>

GLuint load_shader_from_buffer(const char* source, GLenum type)
{
    std::vector<char*> program;
    size_t source_len = strlen(source);
    size_t line_start = 0;
    for (size_t i = 0; i < source_len; ++i)
    {
        if (source[i] == '\n')
        {
            size_t len = i - line_start;
            if (len > 0)
            {
                len += 1; // include '\n' character
                char* line = new char[len + 1];
                strncpy(line, source + line_start, len);
                line[len] = '\0';
                program.push_back(line);
            }
            line_start = i + 1;
        }
    }
    
    GLuint shader_id = ::glCreateShader(type);
    ::glShaderSource(shader_id, static_cast<GLsizei>(program.size()), program.data(), nullptr);
    ::glCompileShader(shader_id);
    
    GLint status = 0;
    ::glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE)
    {
        GLint info_len = 0;
        ::glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
        
        char* msg = new char[info_len + 1];
        ::glGetShaderInfoLog(shader_id, info_len + 1, nullptr, msg);
        
        log(fmt::format("Failed to compile shader: {}", msg));
        
        delete[] msg;
    }
    
    for (char* line : program)
        delete[] line;
    return shader_id;
}

GLuint load_shader_from_file(const char* filename, GLenum type)
{
    FILE* f = fopen(filename, "rb");
    if (f != nullptr)
    {
        fseek(f, 0, SEEK_END);
        size_t n = static_cast<size_t>(ftell(f));
        fseek(f, 0, SEEK_SET);
        
        std::string s(n, '\0');
        fread(s.data(), n, 1, f);
        GLuint shader_id = load_shader_from_buffer(s.data(), type);
        
        fclose(f);
        return shader_id;
    }
    return 0;
}
