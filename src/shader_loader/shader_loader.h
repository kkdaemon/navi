#pragma once

#include "opengl.h"

GLuint load_shader_from_buffer(const char* source, GLenum type);
GLuint load_shader_from_file(const char* filename, GLenum type);
