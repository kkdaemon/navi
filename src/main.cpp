#include <SDL.h>
#include <SDL_main.h>

#if defined(__BZ_ENGINE_WIN32__)
#   include <glew.h>
#endif

#include "globals.h"

int main(int argc, char** argv)
{
    ::SDL_Init(SDL_INIT_EVERYTHING);

    ::SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    ::SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    ::SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    const Uint32 sdl_window_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI;
    SDL_Window* sdl_window = ::SDL_CreateWindow("Navi v0.1",
                                    SDL_WINDOWPOS_CENTERED,
                                    SDL_WINDOWPOS_CENTERED,
                                    800,
                                    600,
                                    sdl_window_flags);

    SDL_GLContext gl_context = ::SDL_GL_CreateContext(sdl_window);
#if defined(__BZ_ENGINE_WIN32__)
    ::glewInit();
#endif

    ::SDL_GL_SetSwapInterval(1);
    
    globals::init();
    
    ::SDL_GetWindowSize(sdl_window, &g_global->window_width, &g_global->window_height);
    g_global->window_size_changhed = true;

    bool running = true;
    while(running)
    {
        SDL_Event e;
        while(::SDL_PollEvent(&e))
        {
            switch(e.type)
            {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_WINDOWEVENT:
                if (e.window.event == SDL_WINDOWEVENT_RESIZED || e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
                {
                    g_global->window_width = e.window.data1;
                    g_global->window_height = e.window.data2;
                    g_global->window_size_changhed = true;
                }
                break;
            }
            
            g_global->kbd->handle_event(&e);
        }
        
        world* new_active_world = nullptr;
        size_t nworlds = g_global->worlds.size();
        for (char i = '1';i <= '9' && (i - '0' - 1) < static_cast<char>(nworlds);++i)
        {
            if (g_global->kbd->is_pressed(i))
                new_active_world = g_global->worlds[i - '0' - 1];
        }
        if (new_active_world != nullptr && g_global->active_world != new_active_world)
            g_global->active_world = new_active_world;

        if (g_global->active_world != nullptr)
        {
            g_global->active_world->begin_frame();
            g_global->active_world->render(0.f);
            g_global->active_world->end_frame();
        }
        
        g_global->kbd->end_frame();
        g_global->window_size_changhed = false;

        ::SDL_GL_SwapWindow(sdl_window);
    }

    globals::term();

    ::SDL_GL_DeleteContext(gl_context);
    ::SDL_DestroyWindow(sdl_window);
    ::SDL_Quit();
    return 0;
}
