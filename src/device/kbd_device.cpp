#include "device/kbd_device.h"

#include <cstring>

#include <SDL.h>

key_state kbd_device::get(char key) const
{
    int i = key;
    key_state r = kbd_cur_frame[i] ? key_state::pressed : key_state::released;
    if (kbd_cur_frame[i] != kbd_prev_frame[i])
        r = r | key_state::just;
    return r;
}

bool kbd_device::is_pressed(char key) const
{
    key_state s = get(key);
    return (s & key_state::pressed) == key_state::pressed;
}
bool kbd_device::handle_event(const SDL_Event* e)
{
    if (e->type == SDL_KEYDOWN)
    {
        if (e->key.keysym.sym < 256)
            kbd_cur_frame[e->key.keysym.sym] = true;
        return true;
    }
    if (e->type == SDL_KEYUP)
    {
        if (e->key.keysym.sym < 256)
            kbd_cur_frame[e->key.keysym.sym] = false;
        return true;
    }
    return false;
}

void kbd_device::end_frame()
{
    std::memcpy(kbd_prev_frame, kbd_cur_frame, sizeof(kbd_cur_frame));
}
