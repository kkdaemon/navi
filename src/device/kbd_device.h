#pragma once

enum class key_state
{
    released = 0x01,
    pressed = 0x02,
    just = 0x04,
};

inline key_state operator | (key_state l, key_state r)
{
    return static_cast<key_state>(static_cast<int>(l) | static_cast<int>(r));
}
inline key_state operator & (key_state l, key_state r)
{
    return static_cast<key_state>(static_cast<int>(l) & static_cast<int>(r));
}

union SDL_Event;
class kbd_device
{
public:
    key_state get(char key) const;
    bool is_pressed(char key) const;
    bool handle_event(const SDL_Event* e);
    void end_frame();
    
private:
    bool kbd_cur_frame[256] = {};
    bool kbd_prev_frame[256] = {};
};
