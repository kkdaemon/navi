#include "scene_one/scene_one.h"

#include "globals.h"
#include "logger.h"
#include "shader_loader/shader_loader.h"
#include "shape_generators.h"

#include <fmt/format.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

scene_one::scene_one()
{
    init();
}

scene_one::~scene_one()
{
    term();
}

void scene_one::begin_frame()
{
    if (g_global->window_size_changhed)
    {
        const float ratio = float(g_global->window_width) / float(g_global->window_height);
        projection_perspective = glm::perspective(45.0f, ratio, 0.001f, 1000.0f);
        int retine_scale = 1;
#if defined(__BZ_ENGINE_MACOS__)
        retine_scale = 2;
#endif
        ::glViewport(0, 0, g_global->window_width * retine_scale, g_global->window_height * retine_scale);
    }
}

void scene_one::render(float time_delta)
{
    glm::vec3& camera = camera_position;
    if (g_global->kbd->is_pressed('q'))
        angle_z -= 1.f;
    if (g_global->kbd->is_pressed('e'))
        angle_z += 1.f;
    
    if (g_global->kbd->is_pressed('a'))
        angle_y += 1.f;
    if (g_global->kbd->is_pressed('d'))
        angle_y -= 1.f;
    
    if (g_global->kbd->is_pressed('w'))
        angle_x += 1.f;
    if (g_global->kbd->is_pressed('s'))
        angle_x -= 1.f;
    
    if (g_global->kbd->is_pressed('z'))
        camera.z += 0.1f;
    if (g_global->kbd->is_pressed('x'))
        camera.z -= 0.1f;
    
    glm::mat4 model_view = glm::lookAt(
                                       camera_position,
                                       glm::vec3(0.0f),
                                       glm::vec3(0.0f, 1.0f, 0.0f)
                                       );
    
    ::glClear(GL_COLOR_BUFFER_BIT);
    
    ::glUniformMatrix4fv(proj_uniform_index, 1, GL_FALSE, glm::value_ptr(projection_perspective));
    
    glm::mat4 m = glm::rotate(model_view, glm::radians(angle_x), glm::vec3(1.0f, 0.0f, 0.0f));
    m = glm::rotate(m, glm::radians(angle_y), glm::vec3(0.0f, 1.0f, 0.0f));
    m = glm::rotate(m, glm::radians(angle_z), glm::vec3(0.0f, 0.0f, 1.0f));
    ::glUniformMatrix4fv(mv_uniform_index, 1, GL_FALSE, glm::value_ptr(m));
    
    for (auto& sh : shapes)
        sh.draw();
}

void scene_one::end_frame()
{
    
}

void scene_one::init()
{
    ::glClearColor(0.0f, 0.5f, 1.0f, 1.0f);
    
    shapes.push_back(shape{ &generate_plane });
    shapes.push_back(shape{ &generate_axis });
    camera_position = glm::vec3{ 0.0f, 0.0f, 5.0f };
    
    shader_1 = load_shader_from_file("res/scene_one/shaders/shader_1.vert", GL_VERTEX_SHADER);
    shader_2 = load_shader_from_file("res/scene_one/shaders/shader_2.frag", GL_FRAGMENT_SHADER);
    program = create_program({ shader_1, shader_2 });
    
    ::glUseProgram(program);
    
    proj_uniform_index = ::glGetUniformLocation(program, "projectionMatrix");
    mv_uniform_index = ::glGetUniformLocation(program, "modelViewMatrix");
}

void scene_one::term()
{
    ::glDetachShader(program, shader_1);
    ::glDeleteShader(shader_1);
    
    ::glDetachShader(program, shader_2);
    ::glDeleteShader(shader_2);
    
    ::glDeleteProgram(program);
}

GLuint scene_one::create_program(const std::vector<GLuint>& shaders)
{
    GLuint program_id = ::glCreateProgram();
    for (GLuint shader_id : shaders)
        ::glAttachShader(program_id, shader_id);
    
    ::glLinkProgram(program_id);
    return program_id;
}
