#include "opengl.h"
#include "shape.h"

struct triple
{
    struct vertex
    {
        float x, y, z;
    };

    struct e
    {
        e(float* base) : base(base) {}
        e& operator=(const vertex& v) {
            base[0] = v.x;
            base[1] = v.y;
            base[2] = v.z;
            return *this;
        }
        e& operator=(const e& o) {
            base[0] = o.base[0];
            base[1] = o.base[1];
            base[2] = o.base[2];
            return *this;
        }
        float* base;
    };

    triple(float* data)
        : data(data)
    {}
    void set(int index, float x, float y, float z) {
        data[index * 3 + 0] = x;
        data[index * 3 + 1] = y;
        data[index * 3 + 2] = z;
    }
    e operator[](int index) {
        return e{ &data[index * 3] };
    }
    float* data;
};

void generate_plane(shape& sh)
{
    sh.vertices.resize(60);
    sh.vertex_colors.resize(60);
    sh.mode = GL_LINES;

    float* v = sh.vertices.data();
    float* c = sh.vertex_colors.data();
    float x = -2.f;
    float clr = 0.f;
    for (int i = 0; i < 30;i += 6)
    {
        v[i + 0] = x;
        v[i + 1] = 0.f;
        v[i + 2] = -2.f;

        c[i + 0] = clr;
        c[i + 1] = clr;
        c[i + 2] = clr;

        v[i + 3] = x;
        v[i + 4] = 0.f;
        v[i + 5] = 2.f;

        c[i + 3] = clr;
        c[i + 4] = clr;
        c[i + 5] = clr;

        x += 1.f;
        clr += 0.2f;
    }
    float z = -2.f;
    for (int i = 30; i < 60; i += 6)
    {
        v[i + 0] = -2.f;
        v[i + 1] = 0.f;
        v[i + 2] = z;

        c[i + 0] = 0.f;
        c[i + 1] = 1.f;
        c[i + 2] = 0.f;

        v[i + 3] = 2.f;
        v[i + 4] = 0.f;
        v[i + 5] = z;

        c[i + 3] = 1.f;
        c[i + 4] = 0.f;
        c[i + 5] = 0.f;

        z += 1.f;
    }
}

void generate_axis(shape& sh)
{
    sh.vertices.resize(6 * 3);
    sh.vertex_colors.resize(6 * 3);
    sh.mode = GL_LINES;

    const float axis_len = 5.f;
    triple v{ sh.vertices.data() };
    triple c{ sh.vertex_colors.data() };
    v[0] = { -axis_len, 0.f, 0.f };
    v[1] = {  axis_len, 0.f, 0.f };
    c[0] = { 1.f, 0.f, 0.f };
    c[1] = c[0];

    v[2] = { 0.f, -axis_len, 0.f };
    v[3] = { 0.f,  axis_len, 0.f };
    c[2] = { 0.f, 1.f, 0.f };
    c[3] = c[2];

    v[4] = { 0.f, 0.f, -axis_len };
    v[5] = { 0.f, 0.f,  axis_len };
    c[4] = { 0.f, 0.f, 1.f };
    c[5] = c[4];
}

void generate_triangle_from_tutorial(shape& sh)
{
    sh.vertices.resize(9);
    sh.vertex_colors.resize(9);
    sh.mode = GL_TRIANGLES;

    float* triangle = sh.vertices.data();
    triangle[0] = -0.4f;
    triangle[1] = 0.1f;
    triangle[2] = 0.0f;

    triangle[3] = 0.4f;
    triangle[4] = 0.1f;
    triangle[5] = 0.0f;

    triangle[6] = 0.0f;
    triangle[7] = 0.7f;
    triangle[8] = 0.0f;

    float* triangle_color = sh.vertex_colors.data();
    triangle_color[0] = 1.0f; triangle_color[1] = 0.0f; triangle_color[2] = 0.0f;
    triangle_color[3] = 0.0f; triangle_color[4] = 1.0f; triangle_color[5] = 0.0f;
    triangle_color[6] = 0.0f; triangle_color[7] = 0.0f; triangle_color[8] = 1.0f;
}
