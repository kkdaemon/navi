#pragma once

#include <vector>
#include <functional>

struct shape
{
    shape();
    shape(const std::function<void(shape&)>& generator);

    shape(const shape&) = delete;
    shape& operator=(const shape&) = delete;
    shape(shape&&) = default;
    shape& operator=(shape&&) = default;

    void generate(const std::function<void(shape&)>& generator);
    void draw();

    std::vector<float> vertices;
    std::vector<float> vertex_colors;

    GLenum mode;
    GLuint vao[1];
    GLuint vbo[2];
};
