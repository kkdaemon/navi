#include "opengl.h"
#include "shape.h"

shape::shape() = default;

shape::shape(const std::function<void(shape&)>& generator)
{
    generate(generator);
}

void shape::generate(const std::function<void(shape&)>& generator)
{
    generator(*this);

    ::glGenVertexArrays(1, vao);
    ::glGenBuffers(2, vbo);

    ::glBindVertexArray(vao[0]);

    ::glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    ::glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
    ::glEnableVertexAttribArray(0);
    ::glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    ::glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    ::glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertex_colors.size(), vertex_colors.data(), GL_STATIC_DRAW);
    ::glEnableVertexAttribArray(1);
    ::glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void shape::draw()
{
    ::glBindVertexArray(vao[0]);
    ::glDrawArrays(mode, 0, static_cast<GLsizei>(vertices.size()));
}
