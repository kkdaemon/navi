#include "world.h"

#include "opengl.h"
#include "shape.h"

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

class scene_one : public world
{
public:
    scene_one();
    ~scene_one() override;
    void begin_frame() override;
    void render(float time_delta) override;
    void end_frame() override;
    
private:
    struct context_t;
    
    void init();
    void term();
    
    GLuint create_program(const std::vector<GLuint>& shaders);
    
    GLuint program;
    GLuint shader_1;
    GLuint shader_2;
    
    GLint proj_uniform_index;
    GLint mv_uniform_index;
    
    glm::vec3 camera_position;
    glm::mat4 projection_perspective;
    
    float angle_x = 0.f;
    float angle_y = 0.f;
    float angle_z = 0.f;
    std::vector<shape> shapes;
};
