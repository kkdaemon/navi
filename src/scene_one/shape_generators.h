#pragma once

struct shape;

void generate_plane(shape& sh);
void generate_axis(shape& sh);
void generate_triangle_from_tutorial(shape& sh);
