#pragma once

#if defined(__BZ_ENGINE_WIN32__)
#   include <glew.h>
#elif defined(__BZ_ENGINE_MACOS__)
#    include <OpenGL/gl3.h>
#    include <OpenGL/gl3ext.h>
#endif
