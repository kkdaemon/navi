#include "globals.h"
#include "scene_one/scene_one.h"

globals* g_global = nullptr;

world::~world() = default;

void globals::init()
{
    g_global = new globals{};
    g_global->kbd = new kbd_device{};
    
    g_global->worlds.push_back(new scene_one{});
}

void globals::term()
{
    for (world* w : g_global->worlds)
        delete w;
    g_global->worlds.clear();
    delete g_global->kbd;
    delete g_global;
    g_global = nullptr;
}
