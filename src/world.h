#pragma once

struct world
{
    virtual ~world();
    virtual void begin_frame() = 0;
    virtual void render(float time_delta) = 0;
    virtual void end_frame() = 0;
};
