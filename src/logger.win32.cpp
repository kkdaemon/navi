#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <cstdio>
#include <string>

void log(const std::string& message)
{
    ::OutputDebugStringA(message.c_str());
    ::OutputDebugStringA("\n");

    printf("%s\n", message.c_str());
}
