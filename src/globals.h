#pragma once

#include "device/kbd_device.h"
#include "world.h"

#include <vector>

struct globals
{
    bool window_size_changhed = false;
    int window_width = 0;
    int window_height = 0;
    
    kbd_device* kbd = nullptr;
    std::vector<world*> worlds;
    world* active_world = nullptr;
    
    static void init();
    static void term();
};

extern globals* g_global;
