function(bz_create_target_defaults target)
    set_property(TARGET ${target} PROPERTY BZ_MODULE_NAME ${target})
    set_property(TARGET ${target} PROPERTY BZ_SOURCES "src/")
    set_property(TARGET ${target} PROPERTY BZ_SOURCE_EXCLUDES "")
    set_property(TARGET ${target} PROPERTY BZ_SOURCE_EXTENSIONS "*.h;*.hpp;*.c;*.cc;*.cpp")
    set_property(TARGET ${target} PROPERTY BZ_SOURCE_FILTER "")
    set_property(TARGET ${target} PROPERTY BZ_INCLUDE_DIRS "src/")
    set_property(TARGET ${target} PROPERTY BZ_INCLUDE_DIRS_PRIVATE "")
    set_property(TARGET ${target} PROPERTY BZ_DEFINITIONS "")
    set_property(TARGET ${target} PROPERTY BZ_DEFINITIONS_PRIVATE "")
    set_property(TARGET ${target} PROPERTY BZ_LIBS "")
    set_property(TARGET ${target} PROPERTY BZ_LIBS_PRIVATE "")
    set_property(TARGET ${target} PROPERTY BZ_COMPILER_FLAGS "" $<${BZ_LINUX}:-std=c++14 -stdlib=libc++>)
    set_property(TARGET ${target} PROPERTY BZ_COMPILER_FLAGS_PRIVATE "" $<${BZ_LINUX}:-std=c++14 -stdlib=libc++>)
    set_property(TARGET ${target} PROPERTY BZ_LINK_LIBRARIES "")
    set_property(TARGET ${target} PROPERTY BZ_LINK_LIBRARIES_PRIVATE "")

    if (BZ_LINUX) # Probably 'BZ_ANDROID' should be added here as well.
        set_property(TARGET ${target} PROPERTY BZ_LINKER_FLAGS "-Wl,-export-dynamic") #$<${BZ_LINUX}:-export-dynamic> is failing here for some reason.
    else()
        set_property(TARGET ${target} PROPERTY BZ_LINKER_FLAGS "")
    endif()

    set_property(TARGET ${target} PROPERTY BZ_CXX_STANDARD "17")
    set_property(TARGET ${target} PROPERTY BZ_FLAG_TREAT_WARNINGS_AS_ERRORS OFF)

    # These two can be set by module to invoke additional post-build commands
    # See usage examples in modules/thirdparty_qt and modules/thirdparty_physx
    set_property(GLOBAL PROPERTY BZ_CUSTOM_DEPLOY_COMMAND "")
    set_property(GLOBAL PROPERTY BZ_CUSTOM_DEPLOY_ARG "")

    # Property BZ_MODULE_CLASSNAME is intended only for modules which require initialization and are maintained by
    # ModuleManager class. For now module can only be placed in static library. Behaviour is undefined if this
    # property is used with other target types. Refer programs/unittest/modules/testme as module example.
    set_property(TARGET ${target} PROPERTY BZ_MODULE_CLASSNAME "")
endfunction()

function(bz_create_platform_defaults)
    set(BZ_PLATFORMS
        "win32"
        "win10"
        "ios"
        "mac"
        "android"
        "linux"
        CACHE INTERNAL "")

    set(BZ_PLATFORM_CONFIGS
        "BZ_WIN"
        "BZ_APPLE"
        "BZ_POSIX"
        "BZ_WIN32"
        "BZ_WIN32_X86"
        "BZ_WIN32_X64"
        "BZ_WIN10_X86"
        "BZ_WIN10_X86_32"
        "BZ_WIN10_X86_64"
        "BZ_WIN10"
        "BZ_WIN10_ARM"
        "BZ_WIN10_ARM32"
        "BZ_WIN10_ARM64"
        "BZ_MAC"
        "BZ_MAC_X86"
        "BZ_MAC_X86_32"
        "BZ_MAC_X86_64"
        "BZ_IOS"
        "BZ_IOS_ARM"
        "BZ_IOS_ARM_32"
        "BZ_IOS_ARM_64"
        "BZ_ANDROID"
        "BZ_ANDROID_X86"
        "BZ_ANDROID_X86_32"
        "BZ_ANDROID_X86_64"
        "BZ_ANDROID_ARM"
        "BZ_ANDROID_ARM_32"
        "BZ_ANDROID_ARM_64"
        "BZ_LINUX"
        "BZ_LINUX_X86"
        "BZ_LINUX_X86_32"
        "BZ_LINUX_X86_64"
        CACHE INTERNAL "")

    set(BZ_PLATFORM_LIB_FOLDER_NAME "" CACHE INTERNAL "")
    # Do not set BZ_OUTPUT_DIR_* if someone has set CMAKE_LIBRARY_OUTPUT_DIRECTORY, e.g. Android Studio.
    # In the case of Android Studio using such approach allows native code debugging out of the box
    if(CMAKE_LIBRARY_OUTPUT_DIRECTORY)
        set(BZ_OUTPUT_DIR "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}" CACHE PATH "Output directory")
    else()
        set(BZ_OUTPUT_DIR "${CMAKE_BINARY_DIR}/_bin_$<LOWER_CASE:$<CONFIG>>" CACHE PATH "Output directory")
    endif()

    set_property(CACHE BZ_TARGET_PLATFORM PROPERTY STRINGS ${BZ_PLATFORMS})
endfunction()

function(bz_create_compiler_defaults)
    set(BZ_COMPILERS
        "BZ_COMPILER_MSVC"
        "BZ_COMPILER_POSIX"
        "BZ_COMPILER_GCC"
        "BZ_COMPILER_CLANG"
        CACHE INTERNAL "")
endfunction()

function(bz_create_cmake_defaults)
    set(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE INTERNAL "")
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON CACHE INTERNAL "")
    set(CMAKE_INSTALL_PREFIX "" CACHE INTERNAL "")
endfunction()
