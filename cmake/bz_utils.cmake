function(bz_begin_current_target target_name)
    if("${target_name}" STREQUAL "")
        message(FATAL_ERROR "Target name is empty")
    endif()

    get_property(bz_targets_stack GLOBAL PROPERTY "BZ_TARGETS_STACK")
    if(${target_name} IN_LIST bz_targets_stack)
        message(FATAL_ERROR "Project ${PROJECT_NAME} begins target ${target_name} while it's already in stack")
    endif()
    list(APPEND bz_targets_stack ${target_name})
    set_property(GLOBAL PROPERTY "BZ_TARGETS_STACK" ${bz_targets_stack})

    set_property(TARGET ${target_name} PROPERTY BZ_TARGET 1)

    bz_create_target_defaults(${target_name})
    set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${target_name})
endfunction()

function(bz_get_current_target out target)
    if("${target}" STREQUAL "")
        get_property(bz_targets_stack GLOBAL PROPERTY "BZ_TARGETS_STACK")
        list(GET bz_targets_stack -1 last_target)
        if(NOT "${last_target}" STREQUAL "")
            set(${out} ${last_target} PARENT_SCOPE)
        else()
            message(FATAL_ERROR "There are no current targets in stack")
        endif()
    else()
        set(${out} ${target} PARENT_SCOPE)
    endif()
endfunction()

function(bz_top_level_target out target)
    if("${target}" STREQUAL "")
        get_property(bz_targets_stack GLOBAL PROPERTY "BZ_TARGETS_STACK")
        list(GET bz_targets_stack 0 last_target)
        if(NOT "${last_target}" STREQUAL "")
            set(${out} ${last_target} PARENT_SCOPE)
        else()
            message(FATAL_ERROR "There are no current targets in stack")
        endif()
    else()
        set(${out} ${target} PARENT_SCOPE)
    endif()
endfunction()

function(bz_end_current_target target_name)
    if("${target_name}" STREQUAL "")
        message(FATAL_ERROR "Target name is empty")
    endif()

    get_property(bz_targets_stack GLOBAL PROPERTY "BZ_TARGETS_STACK")
    list(GET bz_targets_stack -1 last_target)

    if("${last_target}" STREQUAL "${target_name}")
        list(REMOVE_AT bz_targets_stack -1)
    else()
        message(FATAL_ERROR "Trying to pop target ${target_name}, while last target is ${last_target}")
    endif()

    set_property(GLOBAL PROPERTY "BZ_TARGETS_STACK" ${bz_targets_stack})
endfunction()

# Syntax: bz_collect_sources(TO <var> EXTENSIONS <extensions> SOURCES [[sources]...])
# sources can be specified:
# - as single file (file.cpp) - should match specified extensions
# - as recursive directory (files/) - all file from directory and all subdirectories that are
#   matched specified extensions
# - as single directory (files/*) - all files from directory that are matched
#   specified extensions
macro(bz_collect_sources)
    cmake_parse_arguments(ARG "" "TO" "EXTENSIONS;SOURCES" ${ARGN})

    if("${ARG_TO}" STREQUAL "")
        message(FATAL_ERROR "Params error: TO isn't set")
    endif()

    if("${ARG_EXTENSIONS}" STREQUAL "")
        message(FATAL_ERROR "Params error: EXTENSIONS isn't set")
    endif()

    bz_utils_collect(${ARG_TO} "${${ARG_TO}}" "${ARG_SOURCES}" "${ARG_EXTENSIONS}" ${ARG_RECURSIVE})
endmacro()

# Syntax: bz_apply_filter(TO <var> IGNORED_TO <var> FILTER [[filter]...])
macro(bz_apply_filter)
    cmake_parse_arguments(ARG "" "TO;IGNORED_TO" "FILTER" ${ARGN})

    if("${ARG_TO}" STREQUAL "")
        message(FATAL_ERROR "Params error: TO isn't set")
    endif()

    if("${ARG_IGNORED_TO}" STREQUAL "")
        message(FATAL_ERROR "Params error: IGNORED_TO isn't set")
    endif()

    bz_utils_match_extension_filter(${ARG_TO} ${ARG_IGNORED_TO} "${${ARG_TO}}" "${ARG_FILTER}")
endmacro()

# Trims N times the front word from a string separated with "/" and removes
# the front "/" characters after that.
# (It's usually is used to prepare Visual Studio filters)
function(bz_utils_trim_front_words out source count)
    set(result ${source})
    set(i 0)
    while(${i} LESS ${count})
        math(EXPR i "${i} + 1")
        # removes everything at the front up to a "/" character
        string(REGEX REPLACE "^([^/]+)" "" result "${result}")
        # removes all consecutive "/" characters from the front
        string(REGEX REPLACE "^(/+)" "" result "${result}")
    endwhile()
    set(${out} ${result} PARENT_SCOPE)
endfunction()

function(bz_utils_set_vs_source_group group files)
    bz_utils_trim_front_words(group "${group}" 0)
    # replacing forward slashes with back slashes so
    # filters can be generated (back slash used in parsing...)
    string(REPLACE "/" "\\" filter "${group}")
    source_group("${filter}" FILES ${files})
endfunction()

# Returns a list of subdirectories for a given directory
function(bz_utils_dir_list out dir)
    set(result "")
    file(GLOB subdirs "${dir}/*")
    foreach(subdir ${subdirs})
        if(IS_DIRECTORY ${subdir})
            get_filename_component(dirname ${subdir} NAME)
            list(APPEND result "${dirname}")
        endif()
    endforeach()
    set(${out} ${result} PARENT_SCOPE)
endfunction()

function(bz_utils_collect out to dirs extensions)
    set(result ${to})
    foreach(item ${dirs})
        set(need_recursive TRUE)
        get_filename_component(dir_name ${item} NAME)

        # recursive?
        if("${dir_name}" STREQUAL "*")
            get_filename_component(item ${item} PATH)
            set(item "${item}/")
            set(need_recursive FALSE)
        endif()

        # directory
        get_filename_component(absolute_item ${item} ABSOLUTE)
        if(IS_DIRECTORY ${absolute_item})
            set(found_files "")
            foreach(cur_ext ${extensions})
                file(GLOB cur_files RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "${item}/${cur_ext}")
                list(APPEND found_files ${cur_files})
            endforeach()

            list(APPEND result ${found_files})
            bz_utils_set_vs_source_group(${item} "${found_files}")

            # recursive directory
            if(${need_recursive})
                bz_utils_dir_list(subdirs "${item}")
                foreach(sub_dir ${subdirs})
                    bz_utils_collect(result "${result}" "${item}/${sub_dir}" "${extensions}")
                endforeach()
            endif()
        # single file
        else()
            if(EXISTS ${absolute_item})
                list(APPEND result ${item})
            else()
                message("Source file ${item} was resolved to ${absolute_item}")
                message(FATAL_ERROR "Required source file ${item} does not exists.")
            endif()
        endif()
    endforeach()

    set(${out} ${result} PARENT_SCOPE)
endfunction()

function(bz_utils_match_extension_filter out out_ignored to filter)
    set(result "")
    set(result_ignored "")
    foreach(item ${to})
        get_filename_component(item_ext ${item} EXT)
        string(TOLOWER "${item_ext}" item_ext)
        string(REGEX REPLACE "\\.[^.]*$" "" item_ext "${item_ext}")
        if(NOT "${item_ext}" STREQUAL "")
            if("${item_ext}" IN_LIST filter)
                list(APPEND result ${item})
            else()
                list(APPEND result_ignored ${item})
            endif()
        else()
            list(APPEND result ${item})
        endif()
    endforeach()
    set(${out} ${result} PARENT_SCOPE)
    set(${out_ignored} ${result_ignored} PARENT_SCOPE)
endfunction()

# converts pathes with ../ to absolute pathes
function(bz_utils_normalize_pathes out pathes)
    set(result "")
    foreach(item ${pathes})
        if(${item} MATCHES "\\$<(.*):(.*)>")
            get_filename_component(absolute_item ${CMAKE_MATCH_2} ABSOLUTE)
            list(APPEND result "$<${CMAKE_MATCH_1}:${absolute_item}>")
        elseif(${item} MATCHES "^\\.\\.[\\\\/]")
            get_filename_component(absolute_item ${item} ABSOLUTE)
            list(APPEND result ${absolute_item})
        else()
            list(APPEND result ${item})
        endif()
    endforeach()
    set(${out} ${result} PARENT_SCOPE)
endfunction()

# convert every path to absolute path
function(bz_utils_make_absole_pathes out pathes)
    set(result "")
    foreach(item ${pathes})
        get_filename_component(absolute_item ${item} ABSOLUTE)
        list(APPEND result ${absolute_item})
    endforeach()
    set(${out} ${result} PARENT_SCOPE)
endfunction()

function(bz_utils_print_list)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "" "PREFIX" "")
    foreach(item ${ARG_UNPARSED_ARGUMENTS})
        message("${ARG_PREFIX}${item}")
    endforeach()
endfunction()

# Modify items in list by adding prefix and/or suffix to each of them
# Syntax: bz_utils_modify_pathes(<out_var> <input_list> PREFIX [prefix] SUFFIX [suffix])
function(bz_utils_modify_pathes out input_list)
    cmake_parse_arguments(PARSE_ARGV 2 ARG "" "PREFIX;SUFFIX" "")
    set(result "")
    foreach(item ${input_list})
        list(APPEND result "${ARG_PREFIX}${item}${ARG_SUFFIX}")
    endforeach()
    set(${out} "${result}" PARENT_SCOPE)
endfunction()

# Download sources from internet.
# Work in two modes:
# - download file by URL, unpack it and store to return_dir path to folder with archive content
# - clone GIT repository and store to return_dir path to folder
# Syntax: bz_utils_download_sources(<return_dir> URL <url_to_archive> LEADING_PATH [leading_path])
# Syntax: bz_utils_download_sources(<return_dir> GIT <url_to_git> BRANCH [branch_name] COMMIT [commit_hash])
function(bz_utils_download_sources folder_var)
    cmake_parse_arguments(PARSE_ARGV 1 ARG "" "URL;GIT;LEADING_PATH;BRANCH;COMMIT" "")

    if(NOT ${ARG_URL} STREQUAL "")
        # make unique folder for each URL (need for multiple donwloads)
        string(MD5 unique_folder ${ARG_URL})
        set(unpack_folder "${CMAKE_CURRENT_BINARY_DIR}/unpacks/${unique_folder}")

        get_filename_component(sources_archive ${ARG_URL} NAME)
        set(sources_archive "${CMAKE_CURRENT_BINARY_DIR}/${sources_archive}")

        if(NOT EXISTS "${sources_archive}")
            message(STATUS "Start downloading sources from ${ARG_URL}")
            file(DOWNLOAD ${ARG_URL} "${sources_archive}")
            message(STATUS "Donwloading complete")
        endif()

        make_directory("${unpack_folder}")
        message(STATUS "Extract sources from archive")
        execute_process(
            COMMAND ${CMAKE_COMMAND} -E tar xfz "${sources_archive}"
            WORKING_DIRECTORY "${unpack_folder}"
            RESULT_VARIABLE tar_result)
        if(NOT tar_result EQUAL 0)
            message(FATAL_ERROR "Error during unpacking archive (exit code: ${git_result})")
        endif()

        if(NOT ${ARG_LEADING_PATH} STREQUAL "")
            set(ARG_LEADING_PATH "/${ARG_LEADING_PATH}")
        endif()

        set(${folder_var} "${unpack_folder}${ARG_LEADING_PATH}" PARENT_SCOPE)

    elseif(NOT ${ARG_GIT} STREQUAL "")
        # make unique folder for each GIT url (need for multiple clones)
        string(MD5 unique_folder ${ARG_GIT})
        set(repo_folder "${CMAKE_CURRENT_BINARY_DIR}/repos/${unique_folder}")

        if(NOT EXISTS ${repo_folder})
            message(STATUS "Start cloning repo from ${ARG_GIT}")
            execute_process(
                COMMAND git clone ${ARG_GIT} ${repo_folder}
                RESULT_VARIABLE git_result)
            if(NOT git_result EQUAL 0)
                message(FATAL_ERROR "Error during git clone operation (exit code: ${git_result})")
            endif()
            message(STATUS "Cloning complete")
        else()
            message(STATUS "Repo already exists. Skip cloning")
        endif()

        if(NOT ${ARG_BRANCH} STREQUAL "")
            message(STATUS "Checkout branch ${ARG_BRANCH}")
            execute_process(
                COMMAND git checkout ${ARG_BRANCH}
                WORKING_DIRECTORY ${repo_folder}
                RESULT_VARIABLE git_result)
            if(NOT git_result EQUAL 0)
                message(FATAL_ERROR "Error during git checkout branch (exit code: ${git_result})")
            endif()
        endif()

        if(NOT ${ARG_COMMIT} STREQUAL "")
            message(STATUS "Checkout commit ${ARG_COMMIT}")
            execute_process(
                COMMAND git checkout ${ARG_COMMIT}
                WORKING_DIRECTORY ${repo_folder}
                RESULT_VARIABLE git_result)
            if(NOT git_result EQUAL 0)
                message(FATAL_ERROR "Error during git checkout commit (exit code: ${git_result})")
            endif()
        endif()

        message(STATUS "Start updating repo")
        execute_process(
            COMMAND git pull
            WORKING_DIRECTORY ${repo_folder}
            RESULT_VARIABLE git_result)
        if(NOT git_result EQUAL 0)
            message(FATAL_ERROR "Error during git pull operation (exit code: ${git_result})")
        endif()
        message(STATUS "Updating complete")

        set(${folder_var} "${repo_folder}" PARENT_SCOPE)

    else()
        message(FATAL_ERROR "Wrong arguments")
    endif()
endfunction()

# Syntax: bz_utils_patch(<path_to_sources> <path_to_patch_file>)
function(bz_utils_patch src_path patch_file)
    message(STATUS "Patch sources with ${patch_file}")
    execute_process(
        COMMAND git apply ${patch_file} "--ignore-whitespace" "-v"
        WORKING_DIRECTORY ${src_path}
        RESULT_VARIABLE git_result)
    if(NOT git_result EQUAL 0)
        message(FATAL_ERROR "Error during patching (exit code: ${git_result})")
    endif()
endfunction()

function (bz_assert var_name message)
    if (NOT ${var_name})
         message( FATAL_ERROR ${message} )
    endif()
endfunction()

# Function which generates .cpp file with module instantiation required by ModuleManager class.
# This function should be applied only to top level executable targets
# Usage: bz_autogen_module_manager(<executable target> <required modules>)
function(bz_autogen_module_manager target module_list)
    message(STATUS "Configuring modules which require initialization...")

    # Variables to be replaced by configure_file in module_manager_autogen.cpp.in
    set(MODULE_HEADERS "")
    set(MODULE_INSTANTIATIONS "")

    # Iterate over whole module list which are should be linked to executable.
    foreach(module ${module_list})
        get_property(target_type TARGET ${module} PROPERTY TYPE)
        # CMake issues error when getting property on INTERFACE_LIBRARY, so first check target type
        if (NOT ${target_type} STREQUAL "INTERFACE_LIBRARY")
            # If target has property BZ_MODULE_CLASSNAME then append necessary values to
            # replacement variables (MODULE_HEADERS, MODULE_GETTERS, MODULE_INSTANTIATIONS)
            # Property BZ_MODULE_CLASSNAME is a marker that module requires initialization.
            get_property(module_classname TARGET ${module} PROPERTY BZ_MODULE_CLASSNAME)
            if (module_classname)
                message(STATUS "    module `${module}` with classname `${module_classname}`")

                string(CONFIGURE "#include <${module}/${module}.h>\n" temp)
                string(APPEND MODULE_HEADERS ${temp})
                
                string(CONFIGURE "    modules.emplace_back(Type::instance<${module_classname}>(), std::make_unique<${module_classname}>())\;\n" temp)
                string(APPEND MODULE_INSTANTIATIONS ${temp})
            endif()
        endif()
    endforeach()
    
    set(infile ${BZ_ROOT}/build/templates/module_manager_autogen.cpp.in)
    set(outfile ${CMAKE_CURRENT_BINARY_DIR}/generated/module_manager_autogen.cpp)
    configure_file(${infile}
                   ${outfile}
                   NEWLINE_STYLE UNIX)
    target_sources(${target} PRIVATE ${outfile})
    source_group(generated FILES "${outfile}")

    message(STATUS "Module configuring done.")
endfunction()
