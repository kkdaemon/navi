# Macro `bz_config_toolchain` plugs toolchain file, if neccessary, depending
# on target platform instead of passing toolchain file through command
# line with -DCMAKE_TOOLCHAIN_FILE. This technique seems to work (in macro only).
# bz_config_toolchain should be invoked in bz.cmake as early as possible
# before other cmake and bz functions (such as setting project name, getting or
# setting target properties, etc).
macro(bz_config_toolchain)
    if(${TOOLCHAIN_CONFIGURED})
        return()
    endif()
    
    if(PROJECT_NAME)
        message("Please, include bz.cmake before specifying project name and using other")
        message("cmake and bz functions. Example:")
        message("|    cmake_minimum_required(VERSION 3.11)")
        message("|")
        message("|    include(bz.cmake)            # <-- should always be included before project")
        message("|")
        message("|    project(my_project)")
        message("|")
        message("|    include(bz_platforms.cmake)  # <-- can be included after project")
        message("|")
        message("|    ...")
    	message(FATAL_ERROR "Wrong include place of bz.cmake")
    endif()

    # If BZ_TARGET_PLATFORM is not set then target platform is the host platform
    if (NOT BZ_TARGET_PLATFORM)
        if (WIN32)
            set(BZ_TARGET_PLATFORM "win32" CACHE STRING "")
        elseif (APPLE)
            set(BZ_TARGET_PLATFORM "mac" CACHE STRING "")
        elseif (UNIX)
            set(BZ_TARGET_PLATFORM "linux" CACHE STRING "")
        else()
            set(BZ_TARGET_PLATFORM "" CACHE STRING "")
        endif()
    endif()

    if(${BZ_TARGET_PLATFORM} STREQUAL "ios")
        set(CMAKE_TOOLCHAIN_FILE "${BZ_ROOT}/build/cmake/toolchains/ios.cmake")
        message(STATUS "Using custom toolchain: ${CMAKE_TOOLCHAIN_FILE}")
    elseif(${BZ_TARGET_PLATFORM} STREQUAL "android")
        set(ANDROID_STL "c++_shared")
        set(ANDROID_PLATFORM "android-19")
        set(CMAKE_TOOLCHAIN_FILE "${ANDROID_NDK}/build/cmake/android.toolchain.cmake")
        message(STATUS "Using custom toolchain: ${CMAKE_TOOLCHAIN_FILE}")
    endif()
    set(TOOLCHAIN_CONFIGURED 1)
endmacro()

function(bz_create_platform_enviroment)
    bz_create_platform_defaults()

    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(BZ_64 TRUE CACHE BOOL "")
    endif()

    if(NOT BZ_TARGET_PLATFORM IN_LIST BZ_PLATFORMS)
        message("Please, run cmake with the following param:")
        message("  cmake -DBZ_TARGET_PLATFORM=<platform> ...")
        message("Allowed platforms are:")
        bz_utils_print_list(${BZ_PLATFORMS} PREFIX "  - ")
        message(FATAL_ERROR "Incompleate enviroment")
    else()
        set(shared_defines "")
        list(APPEND shared_defines "$<$<CONFIG:Debug>:DEBUG>")
        list(APPEND shared_defines "$<$<CONFIG:Debug>:_DEBUG>")
        list(APPEND shared_defines "$<$<CONFIG:Debug>:__BZ_ENGINE_DEBUG__>")
        list(APPEND shared_defines "$<$<NOT:$<CONFIG:Debug>>:NDEBUG>")

        if(${BZ_TARGET_PLATFORM} STREQUAL "win32")

            set_property(GLOBAL PROPERTY BZ_PLATFORM_INCLUDE_DIRS "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_EXTENSION "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_FILTER
                ".win"
                ".win32")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_DEFINITIONS
                "UNICODE"
                "NOMINMAX"
                "_UNICODE"
                "_SCL_SECURE_NO_WARNINGS"
                "_CRT_SECURE_NO_WARNINGS"
                "__BZ_ENGINE_WINDOWS__"
                "__BZ_ENGINE_WIN32__"
                ${shared_defines})

            bz_platform_enable_key(BZ_WIN)
            bz_platform_enable_key(BZ_WIN32)
            if(BZ_64)
                bz_platform_enable_key(BZ_WIN32_X64)
                set(BZ_PLATFORM_LIB_FOLDER_NAME "win32_x64" CACHE INTERNAL "")
            else()
                bz_platform_enable_key(BZ_WIN32_X86)
                set(BZ_PLATFORM_LIB_FOLDER_NAME "win32_x86" CACHE INTERNAL "")
            endif()

        elseif(${BZ_TARGET_PLATFORM} STREQUAL "win10")

            set_property(GLOBAL PROPERTY BZ_PLATFORM_INCLUDE_DIRS "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_EXTENSION "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_FILTER
                ".win"
                ".win10")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_DEFINITIONS
                "UNICODE"
                "NOMINMAX"
                "_UNICODE"
                "_SCL_SECURE_NO_WARNINGS"
                "_CRT_SECURE_NO_WARNINGS"
                "__BZ_ENGINE_WINDOWS__"
                "__BZ_ENGINE_WIN_UAP__"
                ${shared_defines})

            set(BZ_PLATFORM_LIB_FOLDER_NAME "win10" CACHE INTERNAL "")

            bz_platform_enable_key(BZ_WIN)
            bz_platform_enable_key(BZ_WIN10)
            message(FATAL_ERROR "More initialization for platform isn't implemented")

        elseif(${BZ_TARGET_PLATFORM} STREQUAL "ios")

            set_property(GLOBAL PROPERTY BZ_PLATFORM_INCLUDE_DIRS "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_EXTENSION "*.m;*.mm")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_FILTER
                ".apple"
                ".posix"
                ".ios")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_DEFINITIONS
                "__BZ_ENGINE_APPLE__"
                "__BZ_ENGINE_POSIX__"
                "__BZ_ENGINE_IOS__"
                ${shared_defines})

            set(BZ_PLATFORM_LIB_FOLDER_NAME "ios" CACHE INTERNAL "")

            bz_platform_enable_key(BZ_POSIX)
            bz_platform_enable_key(BZ_APPLE)
            bz_platform_enable_key(BZ_IOS)

        elseif(${BZ_TARGET_PLATFORM} STREQUAL "mac")

            set_property(GLOBAL PROPERTY BZ_PLATFORM_INCLUDE_DIRS "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_EXTENSION "*.m;*.mm")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_FILTER
                ".apple"
                ".posix"
                ".mac")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_DEFINITIONS
                "__BZ_ENGINE_APPLE__"
                "__BZ_ENGINE_POSIX__"
                "__BZ_ENGINE_MACOS__"
                ${shared_defines})

            set(BZ_PLATFORM_LIB_FOLDER_NAME "mac" CACHE INTERNAL "")

            bz_platform_enable_key(BZ_POSIX)
            bz_platform_enable_key(BZ_APPLE)
            bz_platform_enable_key(BZ_MAC)
            bz_platform_enable_key(BZ_MAC_X86)
            if(BZ_64)
                bz_platform_enable_key(BZ_MAC_X86_64)
            else()
                bz_platform_enable_key(BZ_MAC_X86_32)
            endif()

        elseif(${BZ_TARGET_PLATFORM} STREQUAL "android")

            set_property(GLOBAL PROPERTY BZ_PLATFORM_INCLUDE_DIRS "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_EXTENSION "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_FILTER
                ".posix"
                ".android")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_DEFINITIONS
                "__BZ_ENGINE_POSIX__"
                "__BZ_ENGINE_ANDROID__"
                ${shared_defines})

            set(BZ_PLATFORM_LIB_FOLDER_NAME "android/${CMAKE_ANDROID_ARCH_ABI}" CACHE INTERNAL "")

            bz_platform_enable_key(BZ_POSIX)
            bz_platform_enable_key(BZ_ANDROID)

        elseif(${BZ_TARGET_PLATFORM} STREQUAL "linux")

            set_property(GLOBAL PROPERTY BZ_PLATFORM_INCLUDE_DIRS "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_EXTENSION "")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_SOURCE_FILTER
                ".posix"
                ".linux")
            set_property(GLOBAL PROPERTY BZ_PLATFORM_DEFINITIONS
                "__BZ_ENGINE_POSIX__"
                "__BZ_ENGINE_LINUX__"
                ${shared_defines})

            set(BZ_PLATFORM_LIB_FOLDER_NAME "linux" CACHE INTERNAL "")

            bz_platform_enable_key(BZ_POSIX)
            bz_platform_enable_key(BZ_LINUX)
            if(BZ_64)
                bz_platform_enable_key(BZ_LINUX_X86_64)
            else()
                bz_platform_enable_key(BZ_LINUX_X86_32)
            endif()
        else()
            message(FATAL_ERROR "Unknown platform: ${BZ_TARGET_PLATFORM}")
        endif()
    endif()

    bz_platform_disable_rest_keys()
endfunction()

function(bz_platform_specific_target_config target)
    get_property(target_type TARGET ${target} PROPERTY TYPE)

    if(${BZ_TARGET_PLATFORM} STREQUAL "ios")
        # Properties common for all target types
        set_target_properties(${target} PROPERTIES
            XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY "1,2"        # supported devices: ipad and iphone
            XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET "8.0"    # deployment target
            XCODE_ATTRIBUTE_ENABLE_BITCODE NO
        )

        if(${target_type} STREQUAL "EXECUTABLE")
            # Properties specific for executable target
            set_target_properties(${target} PROPERTIES
                XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY "iPhone Developer"
                XCODE_ATTRIBUTE_OTHER_CODE_SIGN_FLAGS "--deep"
                XCODE_ATTRIBUTE_LD_RUNPATH_SEARCH_PATHS "@executable_path"
            )
        endif()

        if(${target_type} STREQUAL "SHARED_LIBRARY" OR ${target_type} STREQUAL "STATIC_LIBRARY")
            # Here come properties for library targets if any
        endif()
    endif()
endfunction()

function(bz_platform_enable_key key)
    cmake_parse_arguments(PARSE_ARGV 2 ARG "" "" "")
    if("${ARGV0}" IN_LIST BZ_PLATFORM_CONFIGS)
        set(${ARGV0} 1 CACHE INTERNAL "")
    else()
        message("Unknown platform config: ${ARGV0}")
        message("Allowed platforms configs are:")
        bz_utils_print_list(${BZ_PLATFORM_CONFIGS} PREFIX "  - ")
        message(FATAL_ERROR "Unknown platform config")
    endif()
endfunction()

function(bz_platform_disable_rest_keys)
    foreach(item ${BZ_PLATFORM_CONFIGS})
        if("${${item}}" STREQUAL "")
            set(${item} 0 CACHE INTERNAL "")
        endif()
    endforeach(item )
endfunction()

function(bz_create_compiler_enviroment)
    bz_create_compiler_defaults()

    if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" OR "${CMAKE_GENERATOR}" MATCHES "Visual Studio")
        set(BZ_COMPILER_MSVC 1 CACHE INTERNAL "")
    elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        set(BZ_COMPILER_POSIX 1 CACHE INTERNAL "")
        set(BZ_COMPILER_GCC 1 CACHE INTERNAL "")
    elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang" OR "${CMAKE_GENERATOR}" STREQUAL "Xcode")
        set(BZ_COMPILER_POSIX 1 CACHE INTERNAL "")
        set(BZ_COMPILER_CLANG 1 CACHE INTERNAL "")
    else()
        message(FATAL_ERROR "Unsupported compiler: generator=${CMAKE_GENERATOR} compiler=${CMAKE_CXX_COMPILER_ID}")
    endif()

    # set rest compilers from list to 0
    foreach(item ${BZ_COMPILERS})
        if("${${item}}" STREQUAL "")
            set(${item} 0 CACHE INTERNAL "")
        endif()
    endforeach(item)

    # add compiler-dependent configuration
    if(BZ_COMPILER_MSVC)
        set_property(GLOBAL PROPERTY BZ_PLATFORM_COMPILER_FLAGS
            "/MP"
            "/Zi"
            "/EHa")
        if(BZ_64)
            set_property(GLOBAL PROPERTY BZ_PLATFORM_LINKER_FLAGS "")
        else()
            # TODO: is it needed for win10 tho?
            set_property(GLOBAL PROPERTY BZ_PLATFORM_LINKER_FLAGS "/SAFESEH:NO")
        endif()
    elseif(BZ_COMPILER_POSIX)
        set_property(GLOBAL PROPERTY BZ_PLATFORM_COMPILER_FLAGS "-g")
        set_property(GLOBAL PROPERTY BZ_PLATFORM_LINKER_FLAGS "")
    endif()    
endfunction()

# Function bz_init performs onetime initialization of bz environment
# This function is called from bz_begin, if you need to access bz environment 
# before bz_begin or even without ever using bz_begin call it in your CMakeLists.txt
function(bz_init)
    # Setup platform
    get_property(is_bz_platform_initalized GLOBAL PROPERTY "BZ_PLATFORM_INITIALIZED")
    if(NOT "${is_bz_platform_initalized}" STREQUAL "INITIALIZED")
        if (CMAKE_CROSSCOMPILING)
            message(STATUS "Crosscompiling for ${BZ_TARGET_PLATFORM}")
        endif()
        bz_create_platform_enviroment()
        bz_create_compiler_enviroment()
        bz_create_cmake_defaults()

        # set flag that blitz platform was initialized
        set_property(GLOBAL PROPERTY BZ_PLATFORM_INITIALIZED "INITIALIZED")
    endif()
endfunction()
