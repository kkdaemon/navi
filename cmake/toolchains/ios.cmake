set(CMAKE_SYSTEM_NAME Darwin)

set(CMAKE_OSX_SYSROOT "iphoneos")

# These variables set to successfully execute try_compile
set(CMAKE_MACOSX_BUNDLE YES)
set(CMAKE_XCODE_ATTRIBUTE_CODE_SIGNING_REQUIRED "NO")
