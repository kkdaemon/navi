cmake_minimum_required(VERSION 3.11)

get_filename_component(BZ_ROOT "${CMAKE_CURRENT_LIST_DIR}/../.." ABSOLUTE)
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/sanitizers/cmake" ${CMAKE_MODULE_PATH})
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# don't change include order
include("${CMAKE_CURRENT_LIST_DIR}/bz_utils.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/bz_defaults.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/bz_platforms.cmake")

bz_config_toolchain()

# Syntax: bz_begin(<EXE|STATIC|SHARED> NAME [target_name])
# if target_name isn't set it will be taken from ${PROJECT_NAME}
function(bz_begin)
    bz_init()
    set(target_types "EXE;STATIC;SHARED")
    cmake_parse_arguments(ARG "BUNDLE" "NAME" "" ${ARGN})

    set(target_name ${PROJECT_NAME})
    if(NOT ${ARG_NAME} STREQUAL "")
        set(target_name ${ARG_NAME})
    endif()

    list(GET ARG_UNPARSED_ARGUMENTS 0 target_type)

    if(NOT "${target_type}" IN_LIST target_types)
        message("Wrong or unspecified target_type.")
        message("Allowed target types are:")
        bz_utils_print_list(${target_types} PREFIX "  - ")
        message(FATAL_ERROR "Params error")
    endif()

    # On android C++ code for "executable" image compiled only into shared library
    if (${target_type} STREQUAL "EXE" AND ${BZ_TARGET_PLATFORM} STREQUAL "android")
        set(target_type "SHARED")
    endif()

    if(${target_type} STREQUAL "EXE")
        add_executable(${target_name})
        if (BZ_IOS OR (${ARG_BUNDLE} STREQUAL "TRUE" AND BZ_MAC))
            set_property(TARGET ${target_name} PROPERTY MACOSX_BUNDLE TRUE)
        endif()
    elseif(${target_type} STREQUAL "STATIC")
        add_library(${target_name} STATIC)
    elseif(${target_type} STREQUAL "SHARED")
        add_library(${target_name} SHARED)
    else()
        message(FATAL_ERROR "Unknown target type ${}")
    endif()

    if (${CMAKE_GENERATOR} STREQUAL "Xcode")
        set_target_properties(${target_name} PROPERTIES
            XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT[variant=Debug] "dwarf"
            XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT[variant=Release] "dwarf-with-dsym"
            )
    endif()

    message(STATUS "Begin target ${target_name}, ${target_type}")
    bz_begin_current_target(${target_name})
endfunction()

# Syntax: bz_end(TARGET [target])
# current target becomes previous target
function(bz_end)
    cmake_parse_arguments(ARG "" "TARGET" "" ${ARGN})
    bz_get_current_target(target "${ARG_TARGET}")

    if(NOT "${ARG_UNPARSED_ARGUMENTS}" STREQUAL "")
        message(FATAL_ERROR "Params error: passed '${ARG_UNPARSED_ARGUMENTS}', but only TARGET is allowed")
    endif()

    get_property(target_sources TARGET ${target} PROPERTY "BZ_SOURCES")
    get_property(target_source_excludes TARGET ${target} PROPERTY "BZ_SOURCE_EXCLUDES")
    get_property(target_source_extensions TARGET ${target} PROPERTY "BZ_SOURCE_EXTENSIONS")
    get_property(target_source_filter TARGET ${target} PROPERTY "BZ_SOURCE_FILTER")
    get_property(target_include_dirs TARGET ${target} PROPERTY "BZ_INCLUDE_DIRS")
    get_property(target_include_dirs_private TARGET ${target} PROPERTY "BZ_INCLUDE_DIRS_PRIVATE")
    get_property(target_definitions TARGET ${target} PROPERTY "BZ_DEFINITIONS")
    get_property(target_definitions_private TARGET ${target} PROPERTY "BZ_DEFINITIONS_PRIVATE")
    get_property(target_compiler_flags TARGET ${target} PROPERTY "BZ_COMPILER_FLAGS")
    get_property(target_compiler_flags_private TARGET ${target} PROPERTY "BZ_COMPILER_FLAGS_PRIVATE")
    get_property(target_link_libraries TARGET ${target} PROPERTY "BZ_LINK_LIBRARIES")
    get_property(target_link_libraries_private TARGET ${target} PROPERTY "BZ_LINK_LIBRARIES_PRIVATE")
    get_property(target_linker_flags TARGET ${target} PROPERTY "BZ_LINKER_FLAGS")
    get_property(target_cxx_standard TARGET ${target} PROPERTY "BZ_CXX_STANDARD")

    get_property(platform_source_filter GLOBAL PROPERTY "BZ_PLATFORM_SOURCE_FILTER")
    get_property(platform_source_extension GLOBAL PROPERTY "BZ_PLATFORM_SOURCE_EXTENSION")
    get_property(platform_include_dirs GLOBAL PROPERTY "BZ_PLATFORM_INCLUDE_DIRS")
    get_property(platform_definitions GLOBAL PROPERTY "BZ_PLATFORM_DEFINITIONS")
    get_property(platform_compiler_flags GLOBAL PROPERTY "BZ_PLATFORM_COMPILER_FLAGS")
    get_property(platform_linker_flags GLOBAL PROPERTY "BZ_PLATFORM_LINKER_FLAGS")

    set_property(TARGET ${target} PROPERTY CXX_STANDARD ${target_cxx_standard})

    set(srcs "")
    set(srcs_to_exclude "")
    set(srcs_to_ignore "")
    bz_collect_sources(TO srcs EXTENSIONS ${target_source_extensions} ${platform_source_extension} SOURCES ${target_sources})
    bz_collect_sources(TO srcs_to_exclude EXTENSIONS ${target_source_extensions} ${platform_source_extension} SOURCES ${target_source_excludes})

    if(srcs_to_exclude)
        list(REMOVE_ITEM srcs ${srcs_to_exclude})
    endif()

    bz_apply_filter(TO srcs IGNORED_TO srcs_to_ignore FILTER ${platform_source_filter} ${target_source_filter})

    #set_source_files_properties(${srcs_to_ignore} PROPERTIES HEADER_FILE_ONLY TRUE)
    #target_sources(${target} PRIVATE ${srcs_to_ignore})
    target_sources(${target} PRIVATE ${srcs})

    get_property(all_ignored_files GLOBAL PROPERTY BZ_ALL_IGNORED_FILES)
    set(target_filtered_srcs ${all_ignored_files})
    bz_apply_filter(TO target_filtered_srcs IGNORED_TO all_ignored_files FILTER ${target_source_filter})
    bz_utils_make_absole_pathes(srcs_to_ignore "${srcs_to_ignore}")
    list(APPEND all_ignored_files ${srcs_to_ignore})
    set_property(GLOBAL PROPERTY BZ_ALL_IGNORED_FILES ${all_ignored_files})

    target_sources(${target} PRIVATE ${target_filtered_srcs})
    source_group(TREE "${BZ_ROOT}" FILES ${target_filtered_srcs})

    bz_utils_normalize_pathes(target_source_dirs "${target_source_dirs}")
    bz_utils_normalize_pathes(target_include_dirs "${target_include_dirs}")
    bz_utils_normalize_pathes(target_include_dirs_private "${target_include_dirs_private}")

    # add user-specified includes
    target_include_directories(${target} PUBLIC ${target_include_dirs})
    target_include_directories(${target} PRIVATE ${target_include_dirs_private})
    target_include_directories(${target} PRIVATE ${platform_include_dirs})

    # add user-specified definitions
    target_compile_definitions(${target} PUBLIC ${target_definitions})
    target_compile_definitions(${target} PRIVATE ${target_definitions_private})
    target_compile_definitions(${target} PRIVATE ${platform_definitions})

    # add user-specified compiler flags
    target_compile_options(${target} PRIVATE ${platform_compiler_flags})
    target_compile_options(${target} PUBLIC ${target_compiler_flags})
    target_compile_options(${target} PRIVATE ${target_compiler_flags_private})

    # add dependency from user-specified libraries
    foreach(lib ${target_link_libraries})
        target_link_libraries(${target} PUBLIC ${lib})
    endforeach()

    foreach(lib ${target_link_libraries_private})
        target_link_libraries(${target} PRIVATE ${lib})
    endforeach()

    # set user-specified output directory,
    # and add user-specified linker flags
    # Also setup rpath params for those platform
    # that require it
    set_target_properties(${target} PROPERTIES
        BUILD_WITH_INSTALL_RPATH TRUE
        INSTALL_RPATH "@executable_path/;@executable_path/../Frameworks/"
        LIBRARY_OUTPUT_DIRECTORY   ${BZ_OUTPUT_DIR}
        RUNTIME_OUTPUT_DIRECTORY   ${BZ_OUTPUT_DIR}
        LINK_FLAGS "${target_linker_flags} ${platform_linker_flags}")

    bz_platform_specific_target_config(${target})

    # setting up parameters required only by top level targets, 
    # such as: deployment, bundling
    bz_top_level_target(top_level_target "")
    if (${target} STREQUAL ${top_level_target})
        # check if target has BUNDLE flag and setup it
        get_property(is_bundle TARGET ${target} PROPERTY MACOSX_BUNDLE)
        if (${is_bundle})
            if(${BZ_TARGET_PLATFORM} STREQUAL "ios")
                # TODO: find the way to place dylibs in Framework subfolder (@rpath, signing, etc)
                set(BZ_DEPLOY_DIR "${BZ_OUTPUT_DIR}/${target}.app")
            else()
                set(BZ_DEPLOY_DIR "${BZ_OUTPUT_DIR}/${target}.app/Contents/Frameworks")
            endif()
        else()
            set(BZ_DEPLOY_DIR "${BZ_OUTPUT_DIR}")
        endif()

        # create an unique list of all required modules
        get_property(required_targets TARGET ${target} PROPERTY REQUIRED_TARGETS)
        if (DEFINED required_targets)
            list(REMOVE_DUPLICATES required_targets)
        endif()
        
        # check if any required module need to be deployed
        foreach(module ${required_targets})
            get_property(target_type TARGET ${module} PROPERTY TYPE)

            # for shared libraries automatically add
            # command to copy dll/so into output directory
            if(${target_type} STREQUAL "SHARED_LIBRARY")
                get_property(is_bz_target TARGET ${module} PROPERTY BZ_TARGET)
                if("${is_bz_target}" STREQUAL "")
                    add_custom_command(TARGET ${target} POST_BUILD
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:${module}>
                        ${BZ_DEPLOY_DIR}/$<TARGET_FILE_NAME:${module}>)
                endif()
            endif()

            # check if target has its custom deployment command
            # and add it to the top level target
            get_property(target_custom_deploy_command TARGET ${target} PROPERTY TARGET_CUSTOM_DEPLOY_COMMAND_${module})
            get_property(target_custom_deploy_arg TARGET ${target} PROPERTY TARGET_CUSTOM_DEPLOY_ARG_${module})
            if (target_custom_deploy_command)
                add_custom_command(TARGET ${target} POST_BUILD COMMAND ${target_custom_deploy_command} ARGS ${target_custom_deploy_arg} VERBATIM)
            endif()
        endforeach()
        
        if ("core" IN_LIST required_targets)
            # Generate part of ModuleManager only if core module is present
            bz_autogen_module_manager(${target} "${required_targets}")
        endif()
    endif()

    message(STATUS "End target ${target}")
    bz_end_current_target(${target})
endfunction()

# Syntax: bz_require(<module_name> [PRIVATE] FROM [dir] TARGET [target])
function(bz_require)
    cmake_parse_arguments(ARG "PRIVATE" "FROM;TARGET" "" ${ARGN})
    bz_get_current_target(target "${ARG_TARGET}")

    if("${ARG_UNPARSED_ARGUMENTS}" STREQUAL "")
        message(FATAL_ERROR "Params error: module name is empty")
    endif()

    list(GET ARG_UNPARSED_ARGUMENTS 0 module)

    set(module_privacy "PUBLIC")
    if(${ARG_PRIVATE})
        set(module_privacy "PRIVATE")
    endif()

    if("${ARG_FROM}" STREQUAL "")
        set(ARG_FROM "${BZ_ROOT}/modules/${module}")
    endif()

    if(NOT TARGET "${module}")
        set(module_bin_dir "${CMAKE_BINARY_DIR}/modules/${module}")
        add_subdirectory(${ARG_FROM} ${module_bin_dir})
        if(NOT TARGET "${module}")
            message(FATAL_ERROR "Required module from '${ARG_FROM}' doesn't have target '${module}'")
        endif()
    endif()

    get_property(module_type TARGET ${module} PROPERTY TYPE)
    if(NOT ${module_type} STREQUAL "INTERFACE_LIBRARY")
        get_property(module_requirements TARGET ${module} PROPERTY REQUIRED_TARGETS)
        set_property(TARGET ${target} APPEND PROPERTY REQUIRED_TARGETS ${module_requirements})
    endif()

    set_property(TARGET ${target} APPEND PROPERTY REQUIRED_TARGETS ${module})

    get_property(custom_deploy_command GLOBAL PROPERTY BZ_CUSTOM_DEPLOY_COMMAND)
    get_property(custom_deploy_arg GLOBAL PROPERTY BZ_CUSTOM_DEPLOY_ARG)

    if (custom_deploy_command)
    	bz_top_level_target(top_level_target "")
        set_property(TARGET ${top_level_target} PROPERTY TARGET_CUSTOM_DEPLOY_COMMAND_${module} ${custom_deploy_command})
        if (custom_deploy_arg)
            set_property(TARGET ${top_level_target} PROPERTY TARGET_CUSTOM_DEPLOY_ARG_${module} ${custom_deploy_arg})
        endif()
    endif()
    set_property(GLOBAL PROPERTY BZ_CUSTOM_DEPLOY_COMMAND "")

    target_link_libraries(${target} "${module_privacy}" ${module})
endfunction()

# Syntax: bz_set(<key> <values...> [APPEND] TARGET [target])
function(bz_set)
    cmake_parse_arguments(ARG "APPEND" "TARGET" "" ${ARGN})
    bz_get_current_target(target "${ARG_TARGET}")

    list(GET ARG_UNPARSED_ARGUMENTS 0 key)

    get_property(check_exists TARGET ${target} PROPERTY ${key})
    if("${check_exists}" MATCHES "NOTFOUND")
        message(FATAL_ERROR "Unknown BZ property: ${key}")
    endif()

    list(REMOVE_AT ARG_UNPARSED_ARGUMENTS 0)

    if(${ARG_APPEND})
        get_property(values TARGET ${target} PROPERTY ${key})
        list(APPEND values ${ARG_UNPARSED_ARGUMENTS})
        set_property(TARGET ${target} PROPERTY ${key} ${values})
    else()
        set_property(TARGET ${target} PROPERTY ${key} ${ARG_UNPARSED_ARGUMENTS})
    endif()
endfunction()
