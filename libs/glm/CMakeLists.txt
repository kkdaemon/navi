cmake_minimum_required(VERSION 3.11)

add_library("glm" INTERFACE IMPORTED GLOBAL)
target_include_directories("glm" INTERFACE "include")
